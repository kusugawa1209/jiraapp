package com.gss.jira.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("jira")
public class JiraConfig {

	private String sprintsUri;

	private String sprintIssuesUri;

	private String issueUri;

}

package com.gss.jira.common.vo;

import lombok.Data;

@Data
public class Sprint {

	private Long id;

	private String name;
}

package com.gss.jira.common.vo;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
public class SptintList {

	private List<Sprint> sprints = Lists.newArrayList();
}

package com.gss.jira.common.vo;

import java.util.Map;
import java.util.Objects;

import com.google.common.collect.Maps;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SyncIssue extends Issue {

	public SyncIssue(Long id, String key, Double storyPoints, Double estimatedWorkingHours) {
		this.id = id;
		this.key = key;

		if (Objects.nonNull(storyPoints) && storyPoints > 0) {
			fields.put("customfield_10452", storyPoints);
		}

		if (Objects.nonNull(estimatedWorkingHours) && estimatedWorkingHours > 0) {
			fields.put("customfield_10046", estimatedWorkingHours);

			Map<String, String> timeTracking = Maps.newHashMap();
			timeTracking.put("originalEstimate", estimatedWorkingHours + "h");
			fields.put("timetracking", timeTracking);
		}
	}

	private Map<String, Object> fields = Maps.newHashMap();
}

package com.gss.jira.common.vo;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
public class IssueFields {

	private String summary;

	private Double customfield_10452;

	private Double customfield_10310;

	private Double customfield_10046;

	private List<Issue> subtasks = Lists.newArrayList();

	private Issue parent;
}

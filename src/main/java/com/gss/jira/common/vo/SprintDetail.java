package com.gss.jira.common.vo;

import lombok.Data;

@Data
public class SprintDetail {

	private SprintContent contents;

	private Sprint sprint;

}

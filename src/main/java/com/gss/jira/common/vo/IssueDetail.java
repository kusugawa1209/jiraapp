package com.gss.jira.common.vo;

import java.util.Objects;
import java.util.Optional;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class IssueDetail extends Issue {

	private IssueFields fields;

	public Optional<Long> getParentId() {
		Issue parent = this.fields.getParent();
		if (Objects.nonNull(parent)) {
			return Optional.of(parent.getId());
		} else {
			return Optional.empty();
		}
	}
}

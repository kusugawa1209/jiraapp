package com.gss.jira.common.vo;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;
import com.gss.jira.api.utils.JsonUtils;

import lombok.Data;
import lombok.Getter;

@Getter
public class IssueSyncFields {

	private Double customfield_10452;

	private Double customfield_10046;

	private TimeTracking timetracking = new TimeTracking();

	public void setStoryPoints(Double storyPoints) {
		this.customfield_10452 = storyPoints;
	}

	public void setEstimatedWorkingHours(Double estimatedWorkingHours) {
		this.customfield_10046 = estimatedWorkingHours;

		String originalEstimate = estimatedWorkingHours * 3600 + "h";
		this.timetracking.setOriginalEstimate(originalEstimate);
	}

	public String toString() {
		Map<String, Object> output = Maps.newHashMap();
		if (Objects.nonNull(this.customfield_10452)) {
			output.put("customfield_10452", this.customfield_10452);
		}

		if (Objects.nonNull(this.customfield_10046)) {
			output.put("customfield_10046", this.customfield_10046);
		}

		if (StringUtils.isNotBlank(this.timetracking.getOriginalEstimate())) {
			output.put("timetracking", this.timetracking);
		}

		return JsonUtils.toJsonString(output);
	}
}

@Data
class TimeTracking {
	private String originalEstimate;
}
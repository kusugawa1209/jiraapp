package com.gss.jira.common.vo;

import lombok.Data;
import lombok.NonNull;

@Data
public class CustomField {

	@NonNull
	private String id;
	
	@NonNull
	private String value;

}

package com.gss.jira.common.vo;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
public class SprintContent {
	private List<Issue> completedIssues = Lists.newArrayList();

	private List<Issue> incompletedIssues = Lists.newArrayList();
}

package com.gss.jira.common.vo;

import lombok.Data;

@Data
public class Issue {

	protected Long id;

	protected String key;
}

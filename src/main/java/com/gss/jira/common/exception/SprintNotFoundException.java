package com.gss.jira.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SprintNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public SprintNotFoundException(String message) {
		super(message);
	}

	public SprintNotFoundException(Throwable t) {
		super(t);
	}
}

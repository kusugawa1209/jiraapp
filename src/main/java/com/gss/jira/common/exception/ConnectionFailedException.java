package com.gss.jira.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ConnectionFailedException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConnectionFailedException(String message) {
		super(message);
	}

	public ConnectionFailedException(Throwable t) {
		super(t);
	}
}

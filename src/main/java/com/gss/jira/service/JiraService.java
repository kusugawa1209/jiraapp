package com.gss.jira.service;

import java.util.List;
import java.util.Set;

import com.gss.jira.common.exception.ConnectionFailedException;
import com.gss.jira.common.exception.SprintNotFoundException;
import com.gss.jira.common.vo.IssueDetail;
import com.gss.jira.common.vo.Sprint;
import com.gss.jira.common.vo.SyncIssue;

public interface JiraService {

	public List<Sprint> searchSprintsByName(String authToken, String searchName)
			throws ConnectionFailedException, SprintNotFoundException;

	public Set<IssueDetail> findIssuesBySprintId(String authToken, Long sprintId, boolean isRetrieveAll)
			throws ConnectionFailedException;

	public void syncIssues(String authToken, List<SyncIssue> issues);
}

package com.gss.jira.service.provider;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.gss.jira.api.utils.HttpsConnectionUtils;
import com.gss.jira.api.utils.JsonUtils;
import com.gss.jira.api.utils.LogUtils;
import com.gss.jira.common.config.JiraConfig;
import com.gss.jira.common.exception.ConnectionFailedException;
import com.gss.jira.common.exception.SprintNotFoundException;
import com.gss.jira.common.vo.Issue;
import com.gss.jira.common.vo.IssueDetail;
import com.gss.jira.common.vo.Sprint;
import com.gss.jira.common.vo.SprintContent;
import com.gss.jira.common.vo.SprintDetail;
import com.gss.jira.common.vo.SptintList;
import com.gss.jira.common.vo.SyncIssue;
import com.gss.jira.service.JiraService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JiraServiceImpl implements JiraService {

	@Autowired
	private JiraConfig jiraConfig;

	@Override
	public List<Sprint> searchSprintsByName(String authToken, String searchName)
			throws ConnectionFailedException, SprintNotFoundException {
		try {
			HttpsURLConnection connection = HttpsConnectionUtils.createHttpsConnection(jiraConfig.getSprintsUri(),
					HttpMethod.GET, Optional.of(MediaType.APPLICATION_JSON), Optional.of(MediaType.APPLICATION_JSON),
					Optional.of(authToken), Optional.empty());

			if (connection.getResponseCode() != HttpStatus.OK.value()) {
				log.error("Failed : HTTP error code : {}", connection.getResponseCode());

				throw new ConnectionFailedException(connection.getResponseMessage());
			} else {
				String output = IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);

				SptintList sprintList = JsonUtils.toObject(output, SptintList.class);
				List<Sprint> sprints = sprintList.getSprints();

				List<Sprint> matchedSprints = sprints.stream()
						.filter(sprint -> StringUtils.containsIgnoreCase(sprint.getName(), searchName))
						.sorted((s1, s2) -> Long.compare(s1.getId(), s2.getId())).collect(Collectors.toList());

				if (CollectionUtils.isEmpty(matchedSprints)) {
					throw new SprintNotFoundException("找不到名稱有包含「" + searchName + "」的 Sprint。");
				}

				return matchedSprints;
			}
		} catch (IOException e) {
			throw new ConnectionFailedException(e.getMessage());
		}
	}

	@Override
	public Set<IssueDetail> findIssuesBySprintId(String authToken, Long sprintId, boolean isRetrieveAll)
			throws ConnectionFailedException {
		try {
			String sprintIssuesUri = String.format(jiraConfig.getSprintIssuesUri(), sprintId);
			HttpsURLConnection connection = HttpsConnectionUtils.createHttpsConnection(sprintIssuesUri, HttpMethod.GET,
					Optional.of(MediaType.APPLICATION_JSON), Optional.of(MediaType.APPLICATION_JSON),
					Optional.of(authToken), Optional.empty());

			if (connection.getResponseCode() != HttpStatus.OK.value()) {
				log.error("Failed : HTTP error code : {}", connection.getResponseCode());

				throw new ConnectionFailedException(connection.getResponseMessage());
			} else {
				String output = IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);
				SprintDetail sprintDetail = JsonUtils.toObject(output, SprintDetail.class);
				SprintContent sprintContent = sprintDetail.getContents();
				List<Issue> issues = findIssues(sprintContent, isRetrieveAll);

				return issues.parallelStream().flatMap(issue -> {
					Set<IssueDetail> issueDetails = Sets.newHashSet();
					recursivelyFindIssueDetailById(authToken, issue.getId(), issueDetails);
					return issueDetails.parallelStream();
				}).collect(Collectors.toSet());
			}
		} catch (IOException e) {
			throw new ConnectionFailedException(e.getMessage());
		}
	}

	@Override
	public void syncIssues(String authToken, List<SyncIssue> issues) {
		issues.parallelStream().forEach(issue -> {
			try {
				String issueUri = String.format(jiraConfig.getIssueUri(), issue.getId());

				Map<String, Object> fields = Maps.newHashMap();
				fields.put("fields", issue.getFields());

				HttpsURLConnection connection = HttpsConnectionUtils.createHttpsConnection(issueUri, HttpMethod.PUT,
						Optional.empty(), Optional.of(MediaType.APPLICATION_JSON), Optional.of(authToken),
						Optional.of(fields));
				IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);
			} catch (IOException e) {
				LogUtils.logThrowable(e);
			}
		});
	}

	private void recursivelyFindIssueDetailById(String authToken, Long issueId, Set<IssueDetail> issues) {
		try {
			String issueUri = String.format(jiraConfig.getIssueUri(), issueId);
			HttpsURLConnection incompletedIssueConnection = HttpsConnectionUtils.createHttpsConnection(issueUri,
					HttpMethod.GET, Optional.of(MediaType.APPLICATION_JSON), Optional.of(MediaType.APPLICATION_JSON),
					Optional.of(authToken), Optional.empty());
			String incompletedIssueOutput = IOUtils.toString(incompletedIssueConnection.getInputStream(),
					StandardCharsets.UTF_8);

			IssueDetail issueDetail = JsonUtils.toObject(incompletedIssueOutput, IssueDetail.class);
			issues.add(issueDetail);

			List<Issue> subtasks = issueDetail.getFields().getSubtasks();

			if (CollectionUtils.isNotEmpty(subtasks)) {
				subtasks.parallelStream().forEach(subtask -> {
					recursivelyFindIssueDetailById(authToken, subtask.getId(), issues);
				});
			}
		} catch (IOException e) {
			LogUtils.logThrowable(e);
		}
	}

	private List<Issue> findIssues(SprintContent sprintContent, boolean isRetrieveAll) {
		if (!isRetrieveAll) {
			return sprintContent.getIncompletedIssues();
		} else {
			return Lists.newArrayList(sprintContent.getCompletedIssues(), sprintContent.getIncompletedIssues()).stream()
					.flatMap(List::stream).collect(Collectors.toList());
		}
	}
}

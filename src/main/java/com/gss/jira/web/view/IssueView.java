package com.gss.jira.web.view;

import lombok.Data;
import lombok.NonNull;

@Data
public class IssueView {

	@NonNull
	private Long parentId;

	@NonNull
	private Long id;

	@NonNull
	private String key;

	@NonNull
	private String summary;

	@NonNull
	private Double storyPoints;

	@NonNull
	private Double estimatedWorkingHours;

}

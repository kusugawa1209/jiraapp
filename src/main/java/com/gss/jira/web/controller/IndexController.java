package com.gss.jira.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.gss.jira.common.config.ProjectConfig;

@Controller
public class IndexController {

	@Autowired
	private ProjectConfig projectConfig;

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("project", projectConfig);
		return "index";
	}
}

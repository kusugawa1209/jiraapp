package com.gss.jira.web.controller.rest;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gss.jira.api.utils.DoubleUtils;
import com.gss.jira.common.exception.ConnectionFailedException;
import com.gss.jira.common.exception.SprintNotFoundException;
import com.gss.jira.common.vo.IssueDetail;
import com.gss.jira.common.vo.Sprint;
import com.gss.jira.common.vo.SyncIssue;
import com.gss.jira.service.JiraService;
import com.gss.jira.web.view.IssueView;

@RestController
public class JIraController {

	@Autowired
	private JiraService jiraService;

	@GetMapping("/jira/token/{authToken}/sprint/{sprintName}")
	public List<Sprint> findSprint(@PathVariable("authToken") String authToken,
			@PathVariable("sprintName") String sprintName) throws ConnectionFailedException, SprintNotFoundException {
		return jiraService.searchSprintsByName(authToken, sprintName);
	}

	@GetMapping("/jira/token/{authToken}/sprintId/{sprintId}/retrieveAll/{isRetrieveAll}")
	public List<IssueView> findSprintIssues(@PathVariable("authToken") String authToken,
			@PathVariable("sprintId") Long sprintId, @PathVariable("isRetrieveAll") boolean isRetrieveAll)
			throws ConnectionFailedException {
		Set<IssueDetail> issueDetails = jiraService.findIssuesBySprintId(authToken, sprintId, isRetrieveAll);

		return issueDetails.parallelStream().map(issueDetail -> {
			Long id = issueDetail.getId();
			String key = issueDetail.getKey();
			String summary = issueDetail.getFields().getSummary();

			Double storyPoints = issueDetail.getFields().getCustomfield_10452();
			if (Objects.isNull(storyPoints)) {
				storyPoints = 0.0;
			}

			Double estimatedWorkingHours1 = issueDetail.getFields().getCustomfield_10046();

			Double estimatedWorkingHours2 = issueDetail.getFields().getCustomfield_10310();

			Double estimatedWorkingHours = DoubleUtils.max(estimatedWorkingHours1, estimatedWorkingHours2);

			Long parentId = id;
			if (issueDetail.getParentId().isPresent()) {
				parentId = issueDetail.getParentId().get();
			}

			return new IssueView(parentId, id, key, summary, storyPoints, estimatedWorkingHours);
		}).sorted((issue1, issue2) -> {
			if (Long.compare(issue1.getParentId(), issue2.getParentId()) != 0) {
				return Long.compare(issue1.getParentId(), issue2.getParentId());
			} else {
				return Long.compare(issue1.getId(), issue2.getId());
			}
		}).collect(Collectors.toList());
	}

	@PutMapping("jira/token/{authToken}/issues")
	public void syncIssues(@PathVariable("authToken") String authToken, @RequestBody List<IssueView> issueViews) {
		List<SyncIssue> issueDetails = issueViews.parallelStream().map(issueView -> {

			boolean isStoryPointsNotDefined = Objects.isNull(issueView.getStoryPoints())
					|| issueView.getStoryPoints() <= 0;
			boolean isEstimatedWorkingHoursNotDefined = Objects.isNull(issueView.getEstimatedWorkingHours())
					|| issueView.getEstimatedWorkingHours() <= 0;
			if (isStoryPointsNotDefined && isEstimatedWorkingHoursNotDefined) {
				return null;
			}

			return new SyncIssue(issueView.getId(), issueView.getKey(), issueView.getStoryPoints(),
					issueView.getEstimatedWorkingHours());
		}).filter(Objects::nonNull).collect(Collectors.toList());

		jiraService.syncIssues(authToken, issueDetails);
	}
}

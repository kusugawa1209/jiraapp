package com.gss.jira.api.utils;

import static java.math.BigDecimal.ROUND_HALF_UP;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MapUtils2 {

	private static final String DEFAULT_DEPTH_KEY = "\\.";

	/**
	 * 從 {@link Map} 物件中取得 Key 指定的資料值, 並轉為 Long 型態.
	 * 
	 * Tip. 其中如果資料型態為有階層的 {@link Map} 資料型態. 預設採用 "." 字串來 切割資料, 並向下抓取.
	 * 
	 * @param map - the map of source collection.
	 * @param key - the key name.
	 * 
	 * @return the value of key name.
	 */
	public static Long getValueAsLong(Map<String, Object> map, String key) {
		try {
			String value = Objects.toString(getValue(map, key), "0");
			return new BigDecimal(value).setScale(0, ROUND_HALF_UP).longValue();
		} catch (final NumberFormatException nfe) {
			return 0L;
		}
	}

	/**
	 * 從 {@link Map} 物件中取得 Key 指定的資料值.
	 * 
	 * Tip. 其中如果資料型態為有階層的 {@link Map} 資料型態. 預設採用 "." 字串來 切割資料, 並向下抓取.
	 * 
	 * @param map - the map of source collection.
	 * @param key - the key name.
	 * 
	 * @return the value of key name.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getValue(Map<String, Object> map, String key) {
		return (T) getValue(map, key, DEFAULT_DEPTH_KEY);
	}

	/**
	 * 從 {@link Map} 物件中取得 Key 指定的資料值.
	 * 
	 * Tip. 其中如果資料型態為有階層的 {@link Map} 資料型態. 預設採用 depthKey 字串來 切割資料, 並向下抓取.
	 * 
	 * <pre>
	 * Ex. Some {@link Map} object data:
	 * map = { 
	 *   "data" : {
	 *     "code" : "Foo Code", 
	 *     "name" : "Foo Name"
	 *   {
	 * }
	 * 
	 * 1. getValue(map, "data.name", ".");
	 * 
	 *    return "Foo Name" String.
	 * 
	 * 2. getValue(map, "data", ".");
	 * 
	 *    return a {@link Map} Object. 
	 *    { 
	 *       "code" : "Foo Code", 
	 *       "name" : "Foo Name" 
	 *    }
	 * </pre>
	 * 
	 * @param map      - the map of source collection.
	 * @param key      - the key name.
	 * @param depthKey - the depth key.
	 * 
	 * @return the value of key name.
	 */
	@SuppressWarnings("unchecked")
	public static Object getValue(Map<String, Object> map, String key, String depthKey) {

		Object depthObject = null;
		Map<String, Object> depthMap = map;
		String[] depthKeys = key.split(depthKey);

		for (int i = 0; i < depthKeys.length; i++) {

			depthObject = depthMap.get(depthKeys[i]);

			if (depthObject == null) {
				return null;
			}

			if (depthObject instanceof Map) {
				depthMap = (Map<String, Object>) depthObject;

			} else if (i == (depthKeys.length - 1)) {
				return depthObject;
			}
		}
		return depthObject;
	}
}

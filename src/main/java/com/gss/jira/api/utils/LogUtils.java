package com.gss.jira.api.utils;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Stopwatch;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)

public class LogUtils {

	public static void logThrowable(Throwable t) {
		log.error("Encounter {}, cause: {}", t.getClass().getSimpleName(), t.toString());
		t.printStackTrace();
	}

	public static void logStart(Stopwatch watch, String format, Object... arguments) {
		if (watch.isRunning()) {
			watch.stop();
		}

		watch.reset();
		watch.start();

		log.info(format, arguments);
	}

	public static void logStart(Stopwatch watch, String message) {
		if (watch.isRunning()) {
			watch.stop();
		}

		watch.reset();
		watch.start();

		log.info(message);
	}

	public static Stopwatch logStart(String format, Object... arguments) {
		Stopwatch watch = Stopwatch.createStarted();

		log.info(format, arguments);

		return watch;
	}

	public static Stopwatch logStart(String message) {
		Stopwatch watch = Stopwatch.createStarted();

		log.info(message);

		return watch;
	}

	public static void logStop(Stopwatch watch, String format, Object... arguments) {
		watch.stop();

		String formatWithTime = format + ", costs {} mills.";
		Object[] argumentsWithTime = ArrayUtils.add(arguments, watch.elapsed(TimeUnit.MILLISECONDS));

		log.info(formatWithTime, argumentsWithTime);
	}

	public static void logStop(Stopwatch watch, String message) {
		watch.stop();

		String messageWithTime = message + ", costs {} mills.";
		Object[] arguments = new Object[] { watch.elapsed(TimeUnit.MILLISECONDS) };

		log.info(messageWithTime, arguments);
	}

}

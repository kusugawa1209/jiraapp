package com.gss.jira.api.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonUtils {
	private static Gson gson;

	static {
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
	}

	public static <T> T convert(Object model, Class<T> clazz) {

		if (model == null) {
			return null;
		}

		String jsonString = toJsonString(model);
		return toObject(jsonString, clazz);
	}

	public static <T> List<T> convert(Collection<?> source, final Class<T> clazz) {
		if (CollectionUtils.isEmpty(source)) {
			return Collections.emptyList();
		}

		return source.stream().map(object -> convert(object, clazz)).collect(Collectors.toList());
	}

	public static String toJsonString(Object model) {
		return gson.toJson(model);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMapObject(String jsonString) {
		Map<String, Object> map = Maps.newHashMap();
		return (Map<String, Object>) gson.fromJson(jsonString, map.getClass());
	}

	public static <T> T toObject(String jsonString, Class<T> clazz) {
		return gson.fromJson(jsonString, clazz);
	}

	public static <T> List<T> convertToList(Object model, Class<T> clazz) {
		if (model == null) {
			return Lists.newArrayList();
		}

		List<?> objs = JsonUtils.convert(model, List.class);
		return convert(objs, clazz);
	}

	public static <T> List<T> convertToList(String jsonString, Class<T> clazz) {
		if (StringUtils.isBlank(jsonString)) {
			return Lists.newArrayList();
		}

		List<?> objs = JsonUtils.toObject(jsonString, List.class);
		return convert(objs, clazz);
	}

}

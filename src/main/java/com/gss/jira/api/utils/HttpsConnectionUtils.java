package com.gss.jira.api.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Optional;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpsConnectionUtils {

	static {
		try {
			SSLContext sc = getSSLContext();
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((String hostname, SSLSession session) -> true);
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			LogUtils.logThrowable(e);
		}
	}

	public static HttpsURLConnection createHttpsConnection(String uri, HttpMethod httpMethod,
			Optional<MediaType> acceptMediaType, Optional<MediaType> contentMediaType, Optional<String> authToken,
			Optional<Object> data) throws IOException {

		URL url = new URL(uri);
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod(httpMethod.name());
		acceptMediaType.ifPresent(mediaType -> conn.setRequestProperty("Accept", mediaType.toString()));
		contentMediaType.ifPresent(mediaType -> conn.setRequestProperty("Content-Type", mediaType.toString()));
		authToken.ifPresent(token -> conn.setRequestProperty("Authorization", "Basic " + token));

		data.ifPresent(d -> {
			conn.setDoOutput(true);
			try (DataOutputStream dos = new DataOutputStream(conn.getOutputStream())) {
				String jsonParam = JsonUtils.toJsonString(d);
				dos.writeBytes(jsonParam);
				dos.flush();
			} catch (IOException e) {
				LogUtils.logThrowable(e);
			}
		});

		return conn;
	}

	private static SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sc = SSLContext.getInstance("TLSv1.2");
		TrustManager[] allTrustManager = getAllTrustManager();
		sc.init(null, allTrustManager, new java.security.SecureRandom());

		return sc;
	}

	private static TrustManager[] getAllTrustManager() {
		return new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
				// Trust all certifications
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				// Trust all certifications
			}
		} };
	}

}

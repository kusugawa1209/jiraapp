package com.gss.jira.api.utils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.collect.Lists;
import com.google.common.primitives.Doubles;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DoubleUtils {

	public static double tryParse(String input) {
		if (StringUtils.isBlank(input)) {
			return 0.0;
		} else {
			@Nullable
			Double output = Doubles.tryParse(input);

			if (output != null) {
				return output;
			} else {
				return 0.0;
			}
		}
	}

	public static double max(Double... doubles) {
		List<Double> sources = Lists.newArrayList(doubles).parallelStream().map(source -> {
			if (Objects.isNull(source)) {
				return 0.0;
			} else {
				return source;
			}
		}).collect(Collectors.toList());

		double[] targets = new double[sources.size()];
		for (int i = 0; i < sources.size(); i++) {
			double source = sources.get(i);
			targets[i] = source;
		}

		return Doubles.max(targets);
	}
}

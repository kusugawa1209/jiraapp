jiraApp.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'pages/step1.html',
		controller : 'step1Controller'
	}).when('/step1', {
		templateUrl : 'pages/step1.html',
		controller : 'step1Controller'
	}).when('/step2', {
		templateUrl : 'pages/step2.html',
		controller : 'step2Controller'
	}).when('/step3', {
		templateUrl : 'pages/step3.html',
		controller : 'step3Controller'
	});
});

$.blockUI.defaults.message = '<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';
$.blockUI.defaults.css = {
	padding : 0,
	margin : 0,
	width : '30%',
	top : '40%',
	left : '35%',
	textAlign : 'center',
	backgroundColor : 'rgba(255, 255, 255, 0)',
	cursor : 'wait'
};

jiraApp.factory('httpInterceptor', function($q) {
	return {
		request : function(config) {
			$.blockUI();
			return config;
		},
		requestError : function(rejection) {
			$.unblockUI();
			$.notifyError(rejection.data.message);
			return $q.reject(rejection);
		},
		response : function(response) {
			$.unblockUI();
			return response;
		},
		responseError : function(rejection) {
			$.unblockUI();
			$.notifyError(rejection.data.message);
			return $q.reject(rejection);
		}
	};
});

jiraApp.config(function($httpProvider) {
	$httpProvider.interceptors.push('httpInterceptor');
});

jQuery.extend({
	notifySuccess : function(message) {
		$.notify(message, {
			position : 'top center',
			className : 'success'
		});
	},
	notifyError : function(message) {
		$.notify(message, {
			position : 'top center',
			className : 'error'
		});
	}
});
const jiraApp = angular.module('jiraApp', [ 'ngRoute', 'ui.bootstrap', 'ui.sortable' ]);

jiraApp.controller('indexController', function($scope) {
	// create a message to display in our view
	$scope.message = 'index';

	$scope.steps = {
		'step1' : 'Step 1. 輸入帳號、密碼及 Sprint 名稱',
		'step2' : 'Step 2. 選擇 Sprint',
		'step3' : 'Step 3. 估算 Story Point 及工時'
	};

	$scope.stepsArray = Object.keys($scope.steps).map(function(step) {
		return {
			'name' : step,
			'description' : $scope.steps[step]
		};
	});
	$scope.currentStep = $scope.steps.step1;
});

jiraApp.controller('aboutController', function($scope) {
	$scope.message = 'about';
});

jiraApp.controller('contactController', function($scope) {
	$scope.message = 'contact';
});
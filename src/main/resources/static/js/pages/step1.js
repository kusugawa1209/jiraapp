jiraApp.controller('step1Controller', function($scope, $http, $location) {
	$scope.$parent.currentStep = $scope.$parent.steps.step1;
	
	$scope.searchJira = function() {
		const authToken = btoa($scope.account + ':' + $scope.password);
		$scope.$parent.authToken = authToken;
		
		$http
	    .get('/jira/token/' + authToken + '/sprint/' + $scope.sprintName)
	    .then(function(response) {
	    	$scope.$parent.sprints = response.data;
	    	$location.path("/step2");
	    }).catch(function(e) {
	    }).finally(function() {
	    });
	}
});
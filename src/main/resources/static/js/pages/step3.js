jiraApp.controller('step3Controller', function($scope, $http, $location) {
	$scope.$parent.currentStep = $scope.$parent.steps.step3;

	$scope.issues = $scope.$parent.issues;
	$scope.totalSp = calculateTotalSp();
	$scope.totalHr = calculateTotalHr();
	
	$scope.sortableOptions = {
		opacity : 0.5,
		placeholder : "sortable-placeholder"
	}
	
	$scope.downloadIssuesFromJira = function() {
		$http
	    .get('/jira/token/' + $scope.$parent.authToken + '/sprintId/' + $scope.$parent.selectedSprintId)
	    .then(function(response) {
	    	$scope.issues = $scope.mergeIssues($scope.issues, response.data);
	    	$scope.$parent.issues = $scope.issues;
	    	
	    	$scope.totalSp = calculateTotalSp();
	    	$scope.totalHr = calculateTotalHr();
	    	
	    	$.notifySuccess('更新成功！');
	    }).catch(function(e) {
	    }).finally(function() {
	    });
	}

	$scope.syncIssuesToJira = function() {
		$http
	    .put('/jira/token/' + $scope.$parent.authToken + '/issues', $scope.issues)
	    .then(function(response) {
	    	$.notifySuccess('同步成功！');
	    }).catch(function(e) {
	    }).finally(function() {
	    });
	}
	
	$scope.mergeIssues = function(existedIssues, remoteIssues) {
		let remoteIssuesMap = remoteIssues.reduce(function(map, obj) {
    	    map[obj.id] = obj;
    	    return map;
    	}, {});

    	let existedIssuesMap = existedIssues.reduce(function(map, obj) {
    		map[obj.id] = obj;
    		return map;
    	}, {});
    	
    	let mergedIssues = [];
    	for(let remoteIssueId in remoteIssuesMap) {
    		if(!existedIssuesMap[remoteIssueId]) {
    			mergedIssues.push(remoteIssuesMap[remoteIssueId]);
    		}
    	}
    	
    	for(let existedIssueId in existedIssuesMap) {
    		if(remoteIssuesMap[existedIssueId]) {
    			mergedIssues.push(existedIssuesMap[existedIssueId]);
    		}
    	}
    	
    	return mergedIssues;
	}
	
	$scope.removeIssue = function(issueIndex) {
		let issue = $scope.issues[issueIndex];
		let confirmRemoving = confirm("確定要刪除 " + issue.key + " 嗎?");
		if(confirmRemoving) {
			$scope.issues.splice(issueIndex, 1);
			$scope.$parent.issues = $scope.issues;
			
			$scope.totalSp = calculateTotalSp();
			$scope.totalHr = calculateTotalHr();
		}
	}
	
	$scope.calculateSps = function() {
		$scope.totalSp = calculateTotalSp();
	}

	$scope.calculateHrs = function() {
		$scope.totalHr = calculateTotalHr();
	}
	
	function calculateTotalSp() {
		if(!$scope.issues) {
			return 0;
		}
		
		return Object.keys($scope.issues).map(function(index){
		    return +$scope.issues[index].storyPoints;
		}).reduce(function(a,b){ return a + b },0);
	}
	
	function calculateTotalHr() {
		if(!$scope.issues) {
			return 0;
		}
		
		return Object.keys($scope.issues).map(function(index){
			return +$scope.issues[index].estimatedWorkingHours;
		}).reduce(function(a,b){ return a + b },0);
	}
});
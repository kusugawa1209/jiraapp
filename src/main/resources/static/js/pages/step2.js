jiraApp.controller('step2Controller', function($scope, $http, $location) {
	$scope.isRetrieveAll = true;
	$scope.$parent.currentStep = $scope.$parent.steps.step2;
	
	$scope.sprints = $scope.$parent.sprints;
	
	$scope.chooseSprint = function() {
		$scope.$parent.selectedSprintId = $scope.selectedSprintId;
		
		$http
	    .get('/jira/token/' + $scope.$parent.authToken + '/sprintId/' + $scope.selectedSprintId + '/retrieveAll/' + $scope.isRetrieveAll)
	    .then(function(response) {
	    	$scope.$parent.issues = response.data;
	    	$location.path("/step3");
	    }).catch(function(e) {
	    }).finally(function() {
	    });
	}
});